${AnsiColor.BRIGHT_GREEN}
 _____            _
/ _  /_   _ _   _| |
\// /| | | | | | | |
 / //\ |_| | |_| | |
/____/\__,_|\__,_|_|


${AnsiColor.BRIGHT_MAGENTA}::${AnsiColor.BRIGHT_WHITE} Spring Boot Version: ${AnsiColor.BRIGHT_YELLOW}${spring-boot.version}${spring-boot.formatted-version} ${AnsiColor.BRIGHT_MAGENTA}::
::${AnsiStyle.FAINT} https://gitee.com/darkranger/spring-cloud-books/tree/master/spring-cloud-api-gateway ${AnsiColor.BRIGHT_MAGENTA}::
${AnsiColor.BRIGHT_WHITE}